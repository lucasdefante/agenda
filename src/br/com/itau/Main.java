package br.com.itau;


public class Main {

    public static void main(String[] args) {

        // Inicializando sistema adicionando um número de contatos
        Agenda agenda = new Agenda();
        agenda.criarPessoa(IO.iniciarSistema());

        Menu.apresentaMenu(agenda);

    }
}
