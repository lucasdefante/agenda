package br.com.itau;

public class Pessoa {

    protected String nome;
    protected String email;
    protected long numero;

    public Pessoa(String nome, String email, long numero){
        this.nome = nome;
        this.email = email;
        this.numero = numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getNumero() {
        return numero;
    }

    public void setNumero(long numero) {
        this.numero = numero;
    }

    @Override
    public String toString(){
        String str = new String("Nome: "+nome+" / E-mail: "+email+" / Número: "+numero);
        return str;
    }
}
