package br.com.itau;

import java.util.*;

public class IO {

    public static int iniciarSistema() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Quantas pessoas deseja armazenar?");
        return teclado.nextInt();
    }

    public static Map<String, String> digitarInfo() {
        Scanner teclado = new Scanner(System.in);
        Map<String, String> dados = new HashMap<>();

        System.out.println("Digite o nome da pessoa:");
        dados.put("nome", teclado.nextLine());
        System.out.println("Digite seu e-mail:");
        dados.put("email", teclado.nextLine());
        System.out.println("Digite seu número:");
        dados.put("numero", Long.toString(teclado.nextLong()));

        return dados;
    }


    public static String removerPessoa() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Digite o e-mail da pessoa que deseja remover:");
        return teclado.nextLine();
    }

    public static void imprimirPessoa(Pessoa pessoa) {
        System.out.println(pessoa.toString());
    }

    public static String imprimirPessoa() {
        Scanner teclado = new Scanner(System.in);
        System.out.println("Digite o e-mail ou o número da pessoa que deseja consultar:");
        return teclado.nextLine();
    }

    public static int apresentaOpcoes() {
        Scanner teclado = new Scanner(System.in);
        System.out.print("\n*** MENU ***\n"
                + Menu.ADICIONAR.valor + " - Adicionar contato\n"
                + Menu.CONSULTAR.valor + " - Consultar contato\n"
                + Menu.IMPRIMIR_LISTA.valor + " - Imprimir toda a agenda\n"
                + Menu.REMOVER.valor + " - Remover contato\n"
                + Menu.SAIR.valor + " - Sair\n************\nDigite sua opção: ");

        return teclado.nextInt();
    }

    public static void opcaoErrada() {
        System.out.println("Opção inválida!");
    }
}
