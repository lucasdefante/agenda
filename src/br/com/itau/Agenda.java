package br.com.itau;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Agenda {
    private List<Pessoa> agenda = new ArrayList<>();

    public Agenda() {

    }

    public List<Pessoa> getAgenda() {
        return agenda;
    }

    public void setAgenda(List<Pessoa> agenda) {
        this.agenda = agenda;
    }

    public void removerContato() {
        String email = IO.removerPessoa();
        for (Pessoa p : agenda) {
            if (p.getEmail().equals(email)) {
                System.out.print("Pessoa removida: ");
                IO.imprimirPessoa(p);
                agenda.remove(p);
                break;
            }
        }
    }

    public void consultarContato() {
        String chave = IO.imprimirPessoa();
        for (Pessoa p : agenda) {
            if (p.getEmail().equals(chave) || Long.toString(p.getNumero()).equals(chave)) {
                IO.imprimirPessoa(p);
            }
        }
    }

    public void criarPessoa(int qtde) {
        for (int i = 0; i < qtde; i++) {
            criarPessoa();
        }
    }

    public void criarPessoa() {
        Map<String, String> dados = IO.digitarInfo();
        Pessoa p = new Pessoa(dados.get("nome"), dados.get("email"), Long.parseLong(dados.get("numero")));
        agenda.add(p);
    }

    public void imprimirContatos(){
        for (Pessoa p : agenda) {
            IO.imprimirPessoa(p);
        }
    }

}
