package br.com.itau;

public enum Menu {
    ADICIONAR(1), CONSULTAR(2), IMPRIMIR_LISTA(3), REMOVER(4), SAIR(5);

    public final int valor;

    private Menu(int valor) {
        this.valor = valor;
    }

    public static void apresentaMenu(Agenda agenda) {

        while (true) {
            int modo = 0;
            modo = IO.apresentaOpcoes();

            if (modo == Menu.ADICIONAR.valor) {
                agenda.criarPessoa();
            } else if (modo == Menu.CONSULTAR.valor) {
                // Consultar pessoa pelo e-mail ou número
                agenda.consultarContato();
            } else if (modo == Menu.IMPRIMIR_LISTA.valor) {
                // Imprime todos os contatos na agenda
                agenda.imprimirContatos();
            } else if (modo == Menu.REMOVER.valor) {
                // Remover uma pessoa pelo e-mail
                agenda.removerContato();
            } else if (modo == Menu.SAIR.valor) {
                break;
            } else {
                IO.opcaoErrada();
            }
        }
    }
}
